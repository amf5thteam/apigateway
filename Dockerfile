FROM openjdk:17-ea-11-jdk-slim
COPY target/*SNAPSHOT.jar apigateway.jar
ENTRYPOINT ["java", "-jar", "apigateway.jar","--spring.profiles.active=prod"]